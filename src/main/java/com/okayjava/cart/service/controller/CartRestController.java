package com.okayjava.cart.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cart")
public class CartRestController {
	
	@GetMapping("/data")
	public String getData() {
		
		return "CART-SERVICE Data from Cart Microservice";
	}

}
